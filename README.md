# slurm-cluster
Docker local slurm cluster

To run slurm cluster environment you must execute:

     $ docker-compose up -d

To stop it, you must:

     $ docker-compose stop

To check logs:

     $ docker-compose logs -f

     (stop logs with CTRL-c")

To check running containers:

     $ docker-compose ps

Reference: https://medium.com/analytics-vidhya/slurm-cluster-with-docker-9f242deee601
